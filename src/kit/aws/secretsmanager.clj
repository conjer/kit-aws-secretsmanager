(ns kit.aws.secretsmanager
  (:require
   [kit.util :as u]
   [kit.aws.core :as ac]))

(defonce client (atom nil))

(defn init!
  ([]
   (init! {}))
  ([conf]
   (reset! client (ac/make-client
                   (merge {:api :secretsmanager} conf)))))

;; (init!)

(defn get-secret-value [secret-id]
  (-> (ac/invoke @client :get-secret-value {:secret-id secret-id})
      (update :secret-string u/read-json-keywords)))
